### Client APP Description

*ISS-client* gets the current position of the **I**nternation **S**pace **S**tation (**ISS**).
*ISS-client* uses the latest position acquisition of the **ISS** to show images or videos of
 cities within a 600km^2 area.

The current **ISS** is fetched from [iss-api](https://gitlab.com/rodrigo.cespedes/iis-api).
 [iss-api](https://gitlab.com/rodrigo.cespedes/iis-api) sources the current position of **ISS**
 from [iss-now.json](http://api.open-notify.org/iss-now.json).

The media is sourced from [iss-api](https://gitlab.com/rodrigo.cespedes/iis-api).
 [iss-api](https://gitlab.com/rodrigo.cespedes/iis-api) sources media from 
 [pixabay.com](https://pixabay.com/api/docs/) 

### Nice to haves
* You can use whatever framework you want but any of these would be strongly preferred: Angular, Angularjs, React, Vue or Aurelia. Also, it would be great if you avoid using any kind of scaffolding like “ng new”, “npx create-react-app”, “vue ui” or “au new”.
* Think of it as a production-ready application, basically try to add code optimization, https support (without workarounds), easy to deploy, a building workflow, etc.
* If you don’t understand something or you have a blocker, ask us, you won’t “lose points”.
* Think as if you are already part of the team: Document your code and organize it so any other team member can read it and understand it.
* Unit Testing, Functional Testing, Linting, Componentization, or any type of content that adds quality to your work are always welcome.

### Project Documentation
Please refer to the wiki in order to view progress and docs related to this project:
[Project Wiki](https://gitlab.com/rodrigo.cespedes/iss-client/wikis/home)

### Folks that have visibility on this project
* Ezequiel García (_Senior, Front End Engineer_)
* Leonardo Perez Apiwan (_Lead Software Engineer, Front End_)
* Horacio Oliva (_Director of Engineering_)