let fs = require('fs');
let path = require('path');

class IssClientEnvVars {
    constructor(isProduction, envVarID) {
        this.envID = !envVarID ? Math.random().toString(36).substr(2, 5) : envVarID;
        this.isProd = isProduction;
    }

    generateFileContent(envVars) {
        let fileContent = '',
            envVarsName = 'issGlobalVars',
            procuredVars,
            varsKey;

        varsKey = this.isProd ? 'production' : 'develop';
        if (typeof envVars[varsKey] === 'undefined') {
            return;
        }
        procuredVars = envVars[varsKey];
        if (typeof procuredVars.globalName !== 'undefined') {
            envVarsName = procuredVars.globalName.toString();
        }
        envVarsName = envVarsName.length === 0 ? 'issGlobalVars' : envVarsName;

        fileContent += 'window.' + envVarsName;
        fileContent += ' = JSON.parse(\'' + JSON.stringify(procuredVars) + '\');';

        return fileContent;
    }

    writeVars() {
        let envVarsPath = path.resolve('env.json'),
            varsExist,
            issVars,
            content;

        varsExist = fs.existsSync(envVarsPath);
        if (!varsExist) {
            return;
        }
        console.log(this.envID);
        issVars = JSON.parse(fs.readFileSync(envVarsPath));
        content = this.generateFileContent(issVars);

        fs.writeFileSync(
            path.resolve('dist/iss.env.' + this.envID + '.js'),
            content
        );
    }

    // Define `apply` as its prototype method which is supplied with compiler as its argument
    apply(compiler) {
        compiler.hooks.done.tap('MyPlugin', (stats) => {
            this.writeVars();
        });
    }
}
/*
function IssClientEnvVars(envVarID) {
    this.envID = !envVarID ? Math.random().toString(36).substr(2, 5) : envVarID;
}

function generateFileContent (envVars) {
    let fileContent = '',
        envVarsName = 'issGlobalVars';

    if (typeof envVars.globalName !== 'undefined') {
        envVarsName = envVars.globalName.toString();
    }
    envVarsName = envVarsName.length === 0 ? 'issGlobalVars' : envVarsName;

    fileContent += 'window.' + envVarsName;
    fileContent += ' = JSON.parse("' + JSON.stringify(envVars) + '");';

    return fileContent;
};

IssClientEnvVars.prototype.writeVars = function () {
    let envVarsPath = path.resolve('env.json'),
        varsExist,
        issVars,
        content;

    varsExist = fs.existsSync(envVarsPath);
    if (!varsExist) {
        return;
    }
    console.log(this.envID);
    issVars = JSON.parse(fs.readFileSync(envVarsPath));
    content = generateFileContent(issVars);

    fs.writeFileSync(
        path.resolve('dist/iss.env.' + this.envID + '.js'),
        JSON.stringify(content)
    );
};

IssClientEnvVars.prototype.apply = function (compiler) {
    console.log(this.envID);
    compiler.plugin('done', this.writeVars);
};
*/
module.exports = IssClientEnvVars;