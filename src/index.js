// Import dependencies
import Vue from 'vue';
import axios from 'axios';

// Determine axios token
function axiosSetup() {
    // Configure axios globally
    window.axios.defaults.headers.common = {
        "Accept": "application/json",
        "Authorization": "Bearer " + window.issEnvVars.token
    };
}

// Attach axios as a global variable and attach the token (the timeout is just in case
// we haven't fetched window.issEnvVars yet, this should never happen, it's just to remove
// this unlikely fragility).
window.axios = axios;
if (!window.issEnvVars.token) {
    setTimeout(() => {
        axiosSetup();
    }, 500);
} else {
    axiosSetup();
}

// Let's get all our vue files
// noinspection JSUnresolvedVariable
let vueFiles = require.context('./js', true, /\.vue$/i);

/**
 * Convert a camel case file name into a snake case name.
 * @param camel - Camel case name
 * @return string - snake case name
 */
function camelToSnake(camel) {
    camel = camel.substring(0, 1).toLowerCase() + camel.substring(1);

    return camel.replace(/([A-Z])/g, function($1){return "-"+$1.toLowerCase();});
}

// Let's loop through all available vue files and declare them as components
vueFiles.keys()
    .map(key => Vue.component(
        camelToSnake(key.split('/').pop().split('.')[0]),
        vueFiles(key).default
    ));

// Let's boot Vue and attach it to our container
new Vue({
    el: '#app-container',

    methods: {
        /**
         * Notify all relevant components to update media to the specified lat and lng
         */
        notifyUpdateMedia: function (lat, lng) {
            this.$emit('update-media', { lat: lat, lng: lng});
        }
    }
});