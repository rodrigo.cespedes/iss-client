// webpack v4
let path = require('path');
let webpack = require('webpack');
let VueLoaderPlugin = require('vue-loader/lib/plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let WebpackMd5Hash = require('webpack-md5-hash');
let MiniCssExtractPlugin = require("mini-css-extract-plugin");
let CleanWebpackPlugin = require('clean-webpack-plugin');
let IssClientEnvVars = require('./build/plugins/IssClientEnvVars');

let inProd = process.env.NODE_ENV === 'production';

let envVarID = Math.random().toString(36).substr(2, 5);

module.exports = {
    mode: inProd ? 'production' : 'development',
    entry: {
        main: [
            './src/index.js',
            './src/scss/main.scss'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.[chunkhash].js'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name(file) {
                        let fileName = file.substring(file.length - 11);

                        if (fileName === 'favicon.png') {
                            return 'images/[name].[ext]';
                        }
                        return 'images/[name].[hash].[ext]';
                    },
                }
            },
            {
                test: /\.s[c|a]ss$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    plugins: [
        new VueLoaderPlugin(),
        new CleanWebpackPlugin('dist', {}),
        new MiniCssExtractPlugin({
            filename: 'style.[contenthash].css',
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: inProd
        }),
        new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './src/index.html',
            filename: 'index.html',
            issEnv: 'iss.env.' + envVarID + '.js'
        }),
        new WebpackMd5Hash(),
        new IssClientEnvVars(inProd, envVarID)
    ]
};